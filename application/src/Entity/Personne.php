<?php 

namespace App\Entity;

class Personne
{
  private $nom;
  private $prenom;
  private $adresse;

  public function __construct($nom, $prenom, $adresse)
  {
    $this->nom      = $nom;
    $this->prenom   = $prenom;
    $this->adresse  = $adresse;
  }

  public function getNom()
  {
    return $this->nom;
  }

  public function setNom()
  {
    $this->nom = $nom;
    return $this;
  }

  public function getPrenom()
  {
    return $this->prenom;
  }

  public function setPrenom()
  {
    $this->prenom = $prenom;
    return $this;
  }

  public function getAdresse()
  {
    return $this->adresse;
  }

  public function setAdresse()
  {
    $this->adresse = $adresse;
    return $this;
  }

  public function toString()
  {
    return $this->nom." ".$this->prenom." ".$this->adresse;
  }

}

?>