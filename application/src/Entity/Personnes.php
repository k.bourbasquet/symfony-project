<?php 

namespace App\Entity;

class Personnes
{
  private $nom;
  private $prenom;
  private $ville;

  public function __construct($nom, $prenom, $ville)
  {
    $this->nom    = $nom;
    $this->prenom = $prenom;
    $this->ville  = $ville;
  }

  public function getNom()
  {
    return this->nom;
  }

  public function setNom()
  {
    $this->nom = $nom;
    return $this
  }

  public function getPrenom()
  {
    return this->prenom;
  }

  public function setPrenom()
  {
    $this->prenom = $prenom;
    return $this
  }

  public function getVille()
  {
    return this->ville;
  }

  public function setVille()
  {
    $this->ville = $ville;
    return $this
  }

}

?>