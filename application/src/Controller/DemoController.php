<?php

namespace App\Controller;

use App\Entity\Personne;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DemoController extends AbstractController
{
  /**
   * @Route("/hello", name="sayHello")
   */
  public function sayHello()
  {
    $semaine = [
      'lundi',
      'mardi',
      'mercredi',
      'jeudi',
      'vendredi'
    ];

    return $this->render('demo/hello.html.twig',
    [
      'nom'     => 'Bourbasquet',
      'prenom'  => 'Kévin',
      'semaine' => $semaine
    ]);
  }

  /**
   * @Route("/persons", name="persons")
   */
  public function persons()
  {
    $p1 = new Personne('Inisan',      'Hervé',   'Lorient' );
    $p2 = new Personne('Bourbasquet', 'Kévin',   'Calan'   );
    $p3 = new Personne('Jean',        'Patrick', 'Guemenée');

    $persons = [$p1, $p2, $p3];

    return $this->render('demo/persons.html.twig',
    [
      'personnes' => $persons,
    ]);
  }

  /**
   * @Route("/welcome", name="welcome")
   */
  public function welcome()
  {
    $message = 'Everybody should play Insurgency Sandstorm !';
    $person  = new Personne('Bourbasquet', 'Kévin', 'Calan');
    $semaine = [
      'lundi',
      'mardi',
      'mercredi',
      'jeudi',
      'vendredi',
      'samedi',
      'dimanche'
    ];

    return $this->render('demo/welcome.html.twig',
    [
      'msg'        => $message,
      'personne'   => $person,
      'jourOuvres' => $semaine
    ]);
  }
}
