# Environnement de développement pour Symfony

## Liste des containers

| Container | Description | Port par défaut |
|---|---|---|
| Symfony | Développement Symfony | 3000|
| pgdb | Moteur de bases de données PostgreSQL | 3001|
| pgadmin | Interface d'administration pour PostgreSQL |5050|
| mariadb | Moteur de bases de données MariaDB |3306|
| phpMyAdmin | Interface d'administration pour MariaDB/MySQL |5051|

*** /!\ - Faites la commande composer install dans le dossier application du container Symfony après avoir démarré les containers. ***

## Démarrage des containers

```bash
docker-compose up -d
```

## Arrêt des containers

```bash
docker-compose down
```

## Accès aux interfaces d'administration

```bash
# pgAdmin
localhost:5050

# phpMyAdmin
localhost:5051
```

## Accès au container Symfony

```bash
docker exec -it CONTAINER bash
cd application
```

## Installation d'un package Symfony

Le container Symfony est pré-installé avec **composer**. Une fois dans le container (et dans le dossier `/application`), vous pouvez installer des **packages** de cette manière :

```bash
composer COMMANDE
```

### Exemple 
```bash
composer require symfony/twig-bundle
composer require symfony/asset
composer require symfony/maker-bundle --dev
```

## Utilisation de la console Symfony

Une fois dans le container (et dans le dossier `/application`), vous pouvez invoquer la **console Symfony** de cette manière :

```bash
bin/console COMMANDE
```

### Exemple

```bash
bin/console make:controller
```

